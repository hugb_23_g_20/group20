from concurrent import futures
import grpc, logging, json, shutil
import protos.mood_music_pb2 as mood_music_pb2
import protos.mood_music_pb2_grpc as mood_music_pb2_grpc
import protos.weatherStation_pb2 as weatherStation_pb2
import protos.weatherStation_pb2_grpc as weatherStation_pb2_grpc

class SongDBServicer(mood_music_pb2_grpc.SongDBServicer):
    """Provides methods that implement functionality of mood music server."""
    def __init__(self, debug = False):
        """Executes the functions to load in all json files"""
        self.debug = debug
        self.soundtracks = self.get_soundtracks_from_json()
        self.songs = self.get_songs_from_json()
        self.moods = self.get_moods_from_json()
        self.tods = self.get_tods_from_json()
        self.accounts = self.get_accounts_from_json()
        self.surveys = self.get_surveys_from_json()
        self.__initialise_logger()
    
    def __initialise_logger(self):
        """Creates logger and sets the format"""
        self.logger = logging.getLogger(__name__)
        formatter = logging.Formatter("%(asctime)s: %(message)s", datefmt='%d-%b-%y %H:%M:%S')
        # formatter = logging.Formatter("%(asctime)s: [PID %(process)d] %(message)s", datefmt='%d-%b-%y %H:%M:%S')
        handler = logging.FileHandler(filename="debug.log", mode="a")
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

    def get_soundtracks_from_json(self):
        """Loads in all soundtracks from the soundtracks.json file"""
        filename = "json_files/soundtracks.json"
        if self.debug:
            filename = "json_files/json_test_files/soundtracks.json" # This should be path to standard boilerplate for tests            
        
        soundtrack_list = []
        try:
            with open(filename) as soundtrack_db_file:
                for item in json.load(soundtrack_db_file):
                    soundtrack = mood_music_pb2.Soundtrack(
                        id=item["id"],
                        user_id=item["user_id"],
                        name=item["name"],
                        mood_id=item["mood_id"],
                        tod_id=item["tod_id"],
                        song_ids=item["song_ids"]
                    )  # Creates a soundtrack item from the file
                    soundtrack_list.append(soundtrack)
        except json.JSONDecodeError:
            pass
        return soundtrack_list
    
    def get_accounts_from_json(self):
        """Loads in all accounts from the accounts.json file"""
        filename = "json_files/accounts.json"
        if self.debug:
            filename = "json_files/json_test_files/accounts.json" # This should be path to standard boilerplate for tests
        account_list = []

        try:
            with open(filename) as account_db_file:
                for item in json.load(account_db_file):
                    account = mood_music_pb2.Account(
                        id=item["id"],
                        username=item["username"],
                        liked_songs=item["liked_songs"],
                        disliked_songs=item["disliked_songs"],
                        soundtracks=item["soundtracks"]
                    )  # Creates a account item from the file
                    account_list.append(account)
        except json.JSONDecodeError:
            pass
        return account_list

    def save_soundtracks_to_json(self):
        """Saves all soundtracks to the soundtracks.json file"""
        soundtrack_data = [
            {
                "id": item.id,
                "user_id": item.user_id,
                "name": item.name,
                "mood_id": item.mood_id,
                "tod_id": item.tod_id,
                "song_ids": list(item.song_ids)
            } for item in self.soundtracks]
        
        filename = "json_files/soundtracks.json"
        if self.debug:
            filename = "json_files/json_test_files/soundtracks.json" # This should be path to standard boilerplate for tests            

        with open(filename, 'w') as soundtrack_db_file:
            json.dump(soundtrack_data, soundtrack_db_file, indent=4)

    def save_accounts_to_json(self):
        """Saves all accounts to the accounts.json file"""
        accounts_data = [
            {
                "id": item.id,
                "username": item.username,
                "liked_songs": list(item.liked_songs),
                "disliked_songs": list(item.disliked_songs),
                "soundtracks": list(item.soundtracks)
            } for item in self.accounts]
        
        filename = "json_files/accounts.json"
        if self.debug:
            filename = "json_files/json_test_files/accounts.json" # This should be path to standard boilerplate for tests

        with open(filename, 'w') as accounts_db_file:
            json.dump(accounts_data, accounts_db_file, indent=4)
    
    def save_survey_to_json(self):
        """Saves survey data to a json file"""
        survey_data = [
            {
                "AID": item.AID,
                "SID": item.SID,
                "MID": item.MID,
                "TID": item.TID,
            } for item in self.surveys]
        
        filename = "json_files/surveys.json"
        if self.debug:
            filename = "json_files/json_test_files/surveys.json" # This should be path to standard boilerplate for tests            

        with open(filename, 'w') as surveys_db_file:
            json.dump(survey_data, surveys_db_file, indent=4)

    def get_songs_from_json(self):
        """Loads in all songs from the songs.json file"""
        filename = "json_files/songs.json"
        if self.debug:
            filename = "json_files/json_test_files/songs.json" # This should be path to standard boilerplate for tests       
        song_list = []
        with open(filename) as song_db_file:
            for item in json.load(song_db_file):
                song = mood_music_pb2.Song(
                    id=item["id"],
                    trackName=item["trackName"],
                    artistName=item["artistName"],
                    artistId=item["artistId"],
                    genreId=item["genraId"],
                    releaseYear=item["releaseYear"]
                )  # Creates a song item from the file
                song_list.append(song)
        return song_list

    def get_moods_from_json(self):
        """Loads in all moods from the moods.json file"""
        filename = "json_files/moods.json"
        if self.debug:
            filename = "json_files/json_test_files/moods.json" # This should be path to standard boilerplate for tests  

        mood_list = []
        with open(filename) as mood_db_file:
            for item in json.load(mood_db_file):
                mood = mood_music_pb2.Mood(
                    id=item["id"],
                    descriptor=item["descriptor"],
                )  # Creates a mood item from the file
                mood_list.append(mood)
        return mood_list

    def get_tods_from_json(self):
        """Loads in all times of day from the tods.json file"""
        filename = "json_files/tods.json"
        if self.debug:
            filename = "json_files/json_test_files/tods.json" # This should be path to standard boilerplate for tests  
        tod_list = []
        with open(filename) as mood_db_file:
            for item in json.load(mood_db_file):
                tod = mood_music_pb2.TimeOfDay(
                    id=item["id"],
                    descriptor=item["descriptor"],
                )  # Creates a TimeOfDay item from the file
                tod_list.append(tod)
        return tod_list

    def get_surveys_from_json(self):
        """Loads in all surveys from the surveys.json file"""
        filename = "json_files/surveys.json"
        if self.debug:
            filename = "json_files/json_test_files/surveys.json" # This should be path to standard boilerplate for tests       
        survey_list = []
        try:
            with open(filename) as survey_db_file:
                for item in json.load(survey_db_file):
                    survey = mood_music_pb2.Survey(
                        AID=item["AID"],
                        SID=item["SID"],
                        MID=item["MID"],
                        TID=item["TID"],
                    )  # Creates a song item from the file
                    survey_list.append(survey)
        except json.JSONDecodeError:
            pass
        except FileNotFoundError:
            pass
        return survey_list

    def GetSong(self, request, context):
        """Returns song if song with id exists, else it returns a empty song"""
        for song in self.songs:
            if song.id == request.id:
                return song
        context.abort(grpc.StatusCode.NOT_FOUND,f'Song with id:{request.id} not found.')
    
    def mood_exists(self, mood_id):
        """Checks if moodwith id mood_id exists"""
        return any(mood.id == mood_id for mood in self.moods)
    
    def tod_exists(self, tod_id):
        """Checks if time of day with id tod_id exists"""
        return any(tod.id == tod_id for tod in self.tods)
    
    def account_exists(self, account_id):
        """Checks if account with id account_id exists"""
        return any(account.id == account_id for account in self.accounts)
    
    def song_exists(self, song_id):
        """Checks if song with id song_id exists"""
        return any(song.id == song_id for song in self.songs)
    
    def GetSoundtrack(self, request, context):
        """Returns song if song with id exists, else it returns a empty song"""
        for soundtrack in self.soundtracks:
            if soundtrack.id == request.id:
                return soundtrack
        context.abort(grpc.StatusCode.NOT_FOUND,f'Soundtrack with id:{request.id} not found.')
        
    def CreateSoundtrack(self, request, context):
        """Creates and saves soundtrack from the request soundtrack, adds the soundtrack
        to the owned list of soundtracks for the user"""
        #Checking of the mood exists
        if not self.mood_exists(request.mood_id):
            context.abort(grpc.StatusCode.NOT_FOUND, f'Mood ID: {request.mood_id} not found.')
         
        #Checking if the tod exists
        if not self.tod_exists(request.tod_id):
            context.abort(grpc.StatusCode.NOT_FOUND,f'Time of Day ID: {request.tod_id} not found')
        
        if not self.account_exists(request.user_id):
            context.abort(grpc.StatusCode.NOT_FOUND,f'Account ID: {request.user_id} not found')
        
        for song in request.song_ids:
            if not self.song_exists(song):
                context.abort(grpc.StatusCode.NOT_FOUND,f'Song ID: {song} not found')

        if len(self.soundtracks) == 0:
            request.id = 0
        else:
            request.id = self.soundtracks[-1].id + 1

        for idx, item in enumerate(self.accounts):
            if item.id == request.user_id:
                self.accounts[idx].soundtracks.append(request.id)

        if request.name == "":
            request.name = f"Soundtrack: {request.id}"
        
        self.soundtracks.append(request)
        self.save_accounts_to_json()
        self.save_soundtracks_to_json() 
        return self.soundtracks[-1]

    def EditSoundtrack(self, request, context):
        """Edits soundtrack that is taken in as request if it exists, does not modify user assignment"""
        if not self.mood_exists(request.mood_id):
            context.abort(grpc.StatusCode.NOT_FOUND,f'Mood ID: {request.mood_id} not found.')

        if not self.tod_exists(request.tod_id):
            context.abort(grpc.StatusCode.NOT_FOUND,f'Time of Day ID: {request.tod_id} not found')

        for song in request.song_ids:
            if not self.song_exists(song):
                context.abort(grpc.StatusCode.NOT_FOUND,f'Song ID: {song} not found')
        
        for idx, item in enumerate(self.soundtracks):
            if item.id == request.id:
                if item.user_id != request.user_id:
                    request.user_id = item.user_id
                self.soundtracks[idx] = request
                self.save_soundtracks_to_json()
                return self.soundtracks[idx]
        context.abort(grpc.StatusCode.NOT_FOUND,f'Soundtrack with id:{request.id} not found.')

    def RemoveSoundtrack(self, request, context):
        """Removes soundtrack with request.id and saves to json, remuves the soundtrack id from user"""
        for idx, item in enumerate(self.soundtracks):
            if item.id == request.id:
                for account in self.accounts:
                    if account.id == item.user_id:
                        account.soundtracks.remove(item.id)
                removed_soundtrack = self.soundtracks.pop(idx)
                self.save_soundtracks_to_json()
                self.save_accounts_to_json()
                return removed_soundtrack
        context.abort(grpc.StatusCode.NOT_FOUND,f'Soundtrack with id:{request.id} not found.')
    
    def GenerateSoundtrack(self, request, context):
        """Generates soundtrack based on requested time of day and/or mood from users liked songs"""
        if not self.mood_exists(request.mood_id):
            context.abort(grpc.StatusCode.NOT_FOUND,f'Mood ID: {request.mood_id} not found.')  

        if not self.tod_exists(request.tod_id):
            context.abort(grpc.StatusCode.NOT_FOUND,f'Time of Day ID: {request.tod_id} not found')

        if not self.account_exists(request.AID):
            context.abort(grpc.StatusCode.NOT_FOUND,f'Account ID: {request.AID} not found')
        
        for account in self.accounts: # Find the user's account
            if account.id == request.AID:
                break
            
        if request.mood_id == 0 and request.tod_id == 0:
            generated_soundtrack = mood_music_pb2.Soundtrack(
                user_id=account.id,
                mood_id=request.mood_id,
                tod_id=request.tod_id,
                song_ids=account.liked_songs
            )
        else:
            generated_soundtrack = mood_music_pb2.Soundtrack(
                user_id=account.id,
                mood_id=request.mood_id,
                tod_id=request.tod_id,
                song_ids=[]
            )
            surveys = list(self.ListSurveys(mood_music_pb2.AccountID(id=request.AID), context))
            for survey in surveys: # Go through every survey entry for user
                if survey.SID in account.liked_songs:
                    # if request generated soundtrack is the same mood and time of day
                    if survey.MID == request.mood_id and survey.TID == request.tod_id:
                        generated_soundtrack.song_ids.append(survey.SID)
                    # if request generated soundtrack is the same time of day but NOT mood
                    elif request.mood_id == 0 and request.tod_id == survey.TID:
                        generated_soundtrack.song_ids.append(survey.SID)
                    # if request generated soundtrack is the same mood but NOT time of day
                    elif request.tod_id == 0 and request.mood_id == survey.MID:
                        generated_soundtrack.song_ids.append(survey.SID)

        return self.CreateSoundtrack(generated_soundtrack, context)
    
    def request_current_weather(self):
        """Fetches current weather data from the weather API"""
        API_KEY = '5a71e072-f58a-486a-85b7-a6703f1b899b'
        SERVICE_URL = 'hugb-service-ep3thvejnq-uc.a.run.app:443'
        try:
            with grpc.secure_channel(SERVICE_URL, grpc.ssl_channel_credentials()) as channel:
                stub = weatherStation_pb2_grpc.WeatherStationStub(channel)
                current_weather = stub.get_current_weather(weatherStation_pb2.Location(lat=64.21, lon=-21.19, api_key=API_KEY))
        except grpc.RpcError as e:
            return e
        return current_weather 
    
    def GenerateWeatherSoundtrack(self, request, context):
        """Generate soundtrack based on current weather, for our purposes we only mapped the current
        weather to three seperate moods, happy if cloud cover is below 50, sad if cloud cover is above 50
        and melancholic if there is rain (100% humidity)"""
        # TODO: Possibly mapping time of day and more weather conditions
        if self.debug:
            current_weather = weatherStation_pb2.CurrentWeather(temp=10, humidity=10, clouds=100, wind=10, windDeg=10)
        else:
            try:
                current_weather = self.request_current_weather()
            except grpc.RpcError as e:
                context.abort(e.code(), e.details())
                
        generator = mood_music_pb2.Generator(AID=request.id, mood_id=1, tod_id=0)
        if current_weather.humidity == 100:
            generator.mood_id = 9
            return self.GenerateSoundtrack(generator, context)
        if current_weather.clouds > 50:
            generator.mood_id = 2
            return self.GenerateSoundtrack(generator, context)
        
        return self.GenerateSoundtrack(generator, context)
            
    def CreateSong(self, request, context):
        """Creates a song only on the server (not saved to the json file) """
        request.id = self.songs[-1].id + 1
        self.songs.append(request)
        return request
    
    def GetAccount(self, request, context):
        """Returns account with id if found"""
        for account in self.accounts:
            if account.id == request.id:
                return account
        context.abort(grpc.StatusCode.NOT_FOUND,f"Account with id {request.id} was not found")
    
    def CreateAccount(self, request, context):
        """Creates and saves account from the request account"""
        if request.username is None or request.username == "":
            context.abort(grpc.StatusCode.INVALID_ARGUMENT,'Username can not be empty')
        
        if len(self.accounts) == 0:
            request.id = 0
        else:
            request.id = self.accounts[-1].id + 1

        self.accounts.append(mood_music_pb2.Account(id=request.id, username=request.username))
        self.save_accounts_to_json()
        return self.accounts[-1]
    
    def EditAccount(self, request, context):
        """Edits account username with the request, later functionality to be added"""
        for idx, item in enumerate(self.accounts):
            if item.id == request.id:
                if request.username == "":
                    context.abort(grpc.StatusCode.INVALID_ARGUMENT,f"Username cannot be empty")
                self.accounts[idx].username = request.username
                self.save_accounts_to_json()
                return self.accounts[idx]
            
        context.abort(grpc.StatusCode.NOT_FOUND,f"Account with id {request.id} was not found")
    
    def RemoveAccount(self, request, context):
        """Removes account with request.id and saves to json"""
        for idx, item in enumerate(self.accounts):
            if item.id == request.id:
                removed_account = self.accounts.pop(idx)
                self.save_accounts_to_json()
                return removed_account
            
        context.abort(grpc.StatusCode.NOT_FOUND,f"Account with id {request.id} was not found")
    
    def Login(self, request, context):
        """If account exists, returns that account, otherwise returns an error"""
        for account in self.accounts:
            if account.username.lower() == request.username.lower():
                return account
        context.abort(grpc.StatusCode.NOT_FOUND,f"Account with username {request.username} was not found")

    def Clear(self, request, context):
        """Clears server of any temporary data"""
        self.__init__()
        shutil.rmtree("json_files/json_test_files")
        shutil.copytree("json_files/json_test_files_boiler", "json_files/json_test_files")
        return mood_music_pb2.Null()
    
    def RateSong(self, request, context):
        """Request contains account id, song id and a bool value, if the bool is
        true that means for account with id, song id is added/moved to liked_songs
        list for that account and saved to database"""
        for account in self.accounts:
            if account.id == request.AID:
                # Check if the song_id exists in the account's liked songs
                if request.SID in account.liked_songs:
                    if not request.liked:
                        # User previously liked the song but now dislikes it
                        account.liked_songs.remove(request.SID)
                        account.disliked_songs.append(request.SID)
                # Check if the song_id exists in the account's disliked songs
                elif request.SID in account.disliked_songs:
                    if request.liked:
                        # User previously disliked the song but now likes it
                        account.disliked_songs.remove(request.SID)
                        account.liked_songs.append(request.SID)
                # If user has not yet rated the song, rate it
                else:
                    if request.liked:
                        account.liked_songs.append(request.SID)
                    else:
                        account.disliked_songs.append(request.SID)
                # If none of these the song is already in the correct list
                self.save_accounts_to_json()
                return account
        
        context.abort(grpc.StatusCode.NOT_FOUND,f"Account with ID {request.AID} was not found.")

    def SongSurvey(self, request, context):
        """Creates and saves survey for each song"""
        # Checking if accounr exists
        if not self.account_exists(request.AID):
            context.abort(grpc.StatusCode.NOT_FOUND, f'Account with id: {request.AID} not found.')
        # Checking if song exists
        if not self.song_exists(request.SID):
            context.abort(grpc.StatusCode.NOT_FOUND, f'Song with id: {request.SID} not found.')
        # Checking if the mood exits
        if not self.mood_exists(request.MID):
            context.abort(grpc.StatusCode.NOT_FOUND, f'Mood with id: {request.MID} not found.')    
        # Checking if the time of day exists
        if not self.tod_exists(request.TID):
            context.abort(grpc.StatusCode.NOT_FOUND, f'Time of Day with id: {request.TID} not found.')

        for idx, item in enumerate(self.surveys):
            if request.AID == item.AID and request.SID == item.SID:
                self.surveys[idx] = request
                self.save_survey_to_json()
                return self.surveys[idx]

        self.surveys.append(request)
        self.save_survey_to_json()
        return self.surveys[-1]
    
    def RemoveSongSurvey(self, request, context):
        """Removes a survey with the specific AID and SID."""
        for idx, survey in enumerate(self.surveys):
            if survey.AID == request.AID and survey.SID == request.SID:
                removed_survey = self.surveys.pop(idx)
                self.save_survey_to_json()
                return removed_survey
        context.abort(grpc.StatusCode.NOT_FOUND, f"Survey with AID {request.AID} and SID {request.SID} was not found")   
    
    def song_similarity(self, song1, song2):
        """Calculates the similarity score of different songs based on various factors"""
        score = 0
        #If artist is similar, +3points
        if song1.artistId == song2.artistId:
            score += 3
        #If genre is similar, +2points
        if song1.genreId == song2.genreId:
            score += 2
        #If year is similar (within 5 years), +1point
        if abs(song1.releaseYear - song2.releaseYear) <= 5:
            score += 1

        return score
    
    def RecommendSimilarSongs(self, request, context):
        """Recommends song based on survey data indicating similar mood and TOD.
        Can change the threshold for similarity"""
        SIMILARITY_THRESHOLD = 1
        if not self.song_exists(request.id):
            context.abort(grpc.StatusCode.NOT_FOUND, f'Song with id: {request.id} not found.')
        song_id = request.id
        # Get survey data for the song to recommend
        surveys_for_song = [survey for survey in self.surveys if survey.SID == song_id]
        mood_ids = [survey.MID for survey in surveys_for_song]
        tod_ids = [survey.TID for survey in surveys_for_song]
        song_scores = {}
        # Calculate similarity based on matching survey data
        for survey in self.surveys:
            if survey.SID != song_id:
                if survey.SID not in song_scores:
                    song_scores[survey.SID] = 0
                if survey.MID in mood_ids:
                    song_scores[survey.SID] += 1
                if survey.TID in tod_ids:
                    song_scores[survey.SID] += 1

        # Get songs with the highest similarity scores
        similar_song_ids = sorted(song_scores, key=song_scores.get, reverse=True)
        similar_songs = [self.GetSong(mood_music_pb2.SongID(id=s_id), context) for s_id in similar_song_ids]
        for song in similar_songs:
            song.score = song_scores[song.id]

        return mood_music_pb2.SongList(songs=[song for song in similar_songs if song.score >= SIMILARITY_THRESHOLD])
    
    def RecommendSongsLikeSoundtrack(self, request, context):
        """Returns a list of songs with the same moodID and TODID as the soundtrack"""
        soundtrack_id = request.id
        selected_soundtrack = None
        mood_id = None
        tod_id = None
        #find the soundtrack with requested ID and extract the mood_id and tod_id of the selected soundtrack
        for soundtrack in self.soundtracks:
            if soundtrack.id == soundtrack_id:
                selected_soundtrack = soundtrack
                mood_id = soundtrack.mood_id
                tod_id = soundtrack.tod_id
                break

        if not selected_soundtrack:
            context.abort(grpc.StatusCode.NOT_FOUND, f'Soundtrack with id: {soundtrack_id} not found.')
        
        # Iterate through the songsurvey data to find matching songs
        similar_songs = []
        for survey in self.surveys:
            if survey.MID == mood_id and survey.TID == tod_id:
                similar_songs.append(self.GetSong(mood_music_pb2.SongID(id=survey.SID), context))

        return mood_music_pb2.SongList(songs=similar_songs)
    
    def RecommendSongsForUser(self, request, context):
        """Reccomends songs similar to those in users liked songs, does not inklude already
        liked songs but can reccomend neutral or already disliked songs, only reccomends
        the 5 most similar songs, but can be adjusted easily later on"""
        RECCOMENDATION_AMOUNT = 5
        current_account = self.GetAccount(request, context)

        liked_songs = current_account.liked_songs
        song_scores = {}

        # Go through each liked song and calculate similarity with other songs
        for liked_song_id in liked_songs:
            liked_song = self.GetSong(mood_music_pb2.SongID(id=liked_song_id), context)

            for song in self.songs:
                if song.id in liked_songs:
                    continue

                similarity_score = self.song_similarity(liked_song, song)

                # Add or update the score for this song in the song_scores dictionary
                if song.id not in song_scores:
                    song_scores[song.id] = similarity_score
                else:
                    song_scores[song.id] += similarity_score

        # Sort the songs based on similarity score in descending order
        recommended_song_ids = sorted(song_scores, key=song_scores.get, reverse=True)

        # Select top 5 songs to recommend
        top_song_ids = recommended_song_ids[:RECCOMENDATION_AMOUNT]
        recommended_songs = [self.GetSong(mood_music_pb2.SongID(id=song_id), context) for song_id in top_song_ids]

        for song in recommended_songs:
            song.score = song_scores[song.id]

        return mood_music_pb2.SongList(songs=recommended_songs)
        
    def Filter(self, request, context):
        """Returns a list of songs that are liked and have the correct mood or tod id"""
        song_list = []
        if not self.account_exists(request.user_id):
            context.abort(grpc.StatusCode.NOT_FOUND,f"Account with id {request.user_id} was not found")

        if request.filter_parameter.lower() == "mood":
            if not self.mood_exists(request.id):
                context.abort(grpc.StatusCode.NOT_FOUND, f'Mood with id: {request.id} not found.')   
            acc = self.GetAccount(mood_music_pb2.AccountID(id=request.user_id), context)
            acc_surveys = [survey for survey in self.surveys if survey.AID == request.user_id]
            for survey in acc_surveys:
                if survey.SID in acc.liked_songs and survey.MID == request.id:
                    song_list.append(self.GetSong(mood_music_pb2.SongID(id=survey.SID), context))
            return song_list
        
        elif request.filter_parameter.lower() == "tod":
            if not self.mood_exists(request.id):
                context.abort(grpc.StatusCode.NOT_FOUND, f'TOD with id: {request.id} not found.')           
            acc = self.GetAccount(mood_music_pb2.AccountID(id=request.user_id), context)
            acc_surveys = [survey for survey in self.surveys if survey.AID == request.user_id]
            for survey in acc_surveys:
                if survey.SID in acc.liked_songs and survey.TID == request.id:
                    song_list.append(self.GetSong(mood_music_pb2.SongID(id=survey.SID), context))
            return song_list
        else:
           context.abort(grpc.StatusCode.INVALID_ARGUMENT, "filter_paramater was not found") 

    def ListSongs(self, request, context):
        """Yields every song that exists in the system"""
        for song in self.songs:
            yield song

    def ListMoods(self, request, context):
        """Yields every mood that exists in the system"""
        for mood in self.moods:
            yield mood

    def ListTODs(self, request, context):
        """Yields every time of day that exists in the system"""
        for tod in self.tods:
            yield tod

    def ListSoundtracks(self, request, context):
        """Yields every soundtrack that exists in the system"""
        for soundtrack in self.soundtracks:
            yield soundtrack

    def ListSurveys(self, request, context):
        """Yields every soundtrack that exists in the system"""
        for survey in self.surveys:
            if survey.AID == request.id:
                yield survey
            

def serve(): # pragma: no cover
    """Opens up a server for the program"""
    server = grpc.server(futures.ThreadPoolExecutor(
        max_workers=10))  # Creates server
    mood_music_pb2_grpc.add_SongDBServicer_to_server(
        SongDBServicer(), server
    )
    server.add_insecure_port("[::]:8080") # Designates a port for the server
    server.start() # Starts server
    server.wait_for_termination()

def test_server_start():
    """Starts up a server for the tests"""
    server = grpc.server(futures.ThreadPoolExecutor(
        max_workers=10))
    
    mood_music_pb2_grpc.add_SongDBServicer_to_server(
        SongDBServicer(True), server
    )

    server.add_insecure_port("[::]:5050")
    server.start()
    
    channel = grpc.insecure_channel("localhost:5050")
    return server, mood_music_pb2_grpc.SongDBStub(channel)

def test_server_stop(server):
    """Stops the server"""
    server.stop(grace=None)

if __name__ == "__main__": # pragma: no cover
    serve()
