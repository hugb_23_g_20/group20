import grpc, random

import protos.mood_music_pb2 as mood_music_pb2
import protos.mood_music_pb2_grpc as mood_music_pb2_grpc
from . import mood_music_server as server

class MoodMusicClient:
    def __init__(self):
        """Opens a channel at port 8080 and a stub from the server"""
        self.channel = grpc.insecure_channel("localhost:8080")
        self.stub = mood_music_pb2_grpc.SongDBStub(self.channel)
        self.user = None

    def get_one_song(self, id):
        """Fetches the song with id from the server if it exists"""
        try:
            songid = mood_music_pb2.SongID(id=id)
            song = self.stub.GetSong(songid)
            return song
        except grpc.RpcError as e:
            raise grpc.RpcError

    def get_all_songs(self):
        """Returns a dictionary of all the songs on record"""
        song_dict = {}
        songs = self.stub.ListSongs(mood_music_pb2.Null())
        for song in songs:
            song_dict[song.id] = {
                "trackName": song.trackName,
                "artistName": song.artistName,
                "artistId": song.artistId,
                "genreId": song.genreId,
                "releaseYear": song.releaseYear,
            }
        return song_dict

    def get_all_moods(self):
        """Returns a dictionary of all the moods on record"""
        mood_dict = {}
        moods = self.stub.ListMoods(mood_music_pb2.Null())
        for mood in moods:
            mood_dict[mood.id] = {
                "descriptor": mood.descriptor,
            }
        return mood_dict

    def get_all_tods(self):
        """Returns a dictionary of all the times of day on record"""
        tod_dict = {}
        tods = self.stub.ListTODs(mood_music_pb2.Null())
        for tod in tods:
            tod_dict[tod.id] = {
                "descriptor": tod.descriptor,
            }
        return tod_dict

    def get_all_soundtracks(self):
        """Returns  a dictionary of all the soundtracks on record"""
        soundtrack_dict = {}
        soundtracks = self.stub.ListSoundtracks(mood_music_pb2.Null())
        for soundtrack in soundtracks:
            soundtrack_dict[soundtrack.id] = {
                "name": soundtrack.name,
                "mood_id": soundtrack.mood_id,
                "tod_id": soundtrack.tod_id,
                "song_ids": soundtrack.song_ids,
            }
        return soundtrack_dict

    def create_song(self, song):
        """Creates song"""
        self.stub.CreateSong(song)

    def add_song(self, soundtrack_id, song_id):
        """Adds the song/songs with song_id to soundtrack with soundtrack_id"""
        try: 
            soundtrack = self.stub.GetSoundtrack(mood_music_pb2.SoundtrackID(id=soundtrack_id))
            if type(song_id) == list: # If song_id is a list
                soundtrack.song_ids.extend(song_id)
            else: # If song_id is a integer
                soundtrack.song_ids.append(song_id)
            return self.stub.EditSoundtrack(soundtrack)
        except grpc.RpcError:
            raise grpc.RpcError

    def remove_song(self, soundtrack_id, song_id):
        """Removes the song/songs with song_id from soundtrack with soundtrack_id"""
        try:
            soundtrack = self.stub.GetSoundtrack(mood_music_pb2.SoundtrackID(id=soundtrack_id))
            if type(song_id) == list: # If song_id is a list
                for item in song_id:
                    soundtrack.song_ids.remove(item)
            else: # If song_id is a integer
                soundtrack.song_ids.remove(song_id) 
            return self.stub.EditSoundtrack(soundtrack)
        except grpc.RpcError:
            raise grpc.RpcError

    def edit_tod(self, soundtrack_id, new_tod_id):
        """Replaces the TOD_id if it is valid and sends the soundtrack to replace the old one"""
        soundtrack = self.stub.GetSoundtrack(mood_music_pb2.SoundtrackID(id=soundtrack_id))
        if new_tod_id in self.get_all_tods():
            soundtrack.tod_id = new_tod_id
        return self.stub.EditSoundtrack(soundtrack)
    
    def edit_mood(self, soundtrack_id, new_mood_id):
        """Replaces the mood_id if it is valid and sends the soundtrack to replace the old one"""
        soundtrack = self.stub.GetSoundtrack(mood_music_pb2.SoundtrackID(id=soundtrack_id))
        if new_mood_id in self.get_all_moods():
            soundtrack.mood_id = new_mood_id
        return self.stub.EditSoundtrack(soundtrack)


    def create_soundtrack(self, soundtrack):
        """Creates a soundtrack in the server if everything checks out"""
        # Check if mood exists
        all_moods = self.get_all_moods()
        if soundtrack.mood_id not in all_moods:
            raise ValueError(f"Mood with ID {soundtrack.mood_id} does not exist.")

        # Check if tod exists
        all_tods = self.get_all_tods()
        if soundtrack.tod_id not in all_tods:
            raise ValueError(f"Time of day with ID {soundtrack.tod_id} does not exist.")

        # If both mood and tod exist, create the soundtrack
        return self.stub.CreateSoundtrack(soundtrack)
    
    def remove_soundtrack(self, soundtrack_id):
        """Removes specific soundtrack"""
        soundtrack = self.stub.GetSoundtrack(
            mood_music_pb2.SoundtrackID(id=soundtrack_id))
        return self.stub.RemoveSoundtrack(soundtrack)
    
    def select_mood(self):
        #Available moods
        moods = self.get_all_moods()

        print("Select a mood from the list:")
        for i, (key, value) in enumerate(moods.items()):
            print(f"{i}. {value['descriptor']}")
        selection = int(input("Enter the id of your mood choice: "))
        mood_id = list(moods.keys())[selection]

        return mood_id
    
    def select_tod(self):
        #Available
        tods = self.get_all_tods()

        print("Select a time of day from the list:")
        for i, (key, value) in enumerate(tods.items()):
            print(f"{i}. {value['descriptor']}")

        selection = int(input("Enter the id of the time of day: "))
        tod_id = list(tods.keys())[selection]

        return tod_id
    
    def filter_tod_soundtracks(self, tod_id):
        """Takes in a tod id and returns all soundtracks with said tod id"""
        filtered_tod_soundtrack_dict = {}
        soundtracks = self.stub.ListSoundtracks(mood_music_pb2.Null())
        for soundtrack in soundtracks:
            if tod_id == soundtrack.tod_id:    
                filtered_tod_soundtrack_dict[soundtrack.id] = {
                    "name": soundtrack.name,
                    "mood_id": soundtrack.mood_id,
                    "tod_id": soundtrack.tod_id,
                    "song_ids": soundtrack.song_ids,
                }
        return filtered_tod_soundtrack_dict
    
    def filter_mood_soundtracks(self, mood_id):
        """Takes in a mood id and returns all soundtracks with said mood id"""
        filtered_mood_soundtrack_dict = {}
        soundtracks = self.stub.ListSoundtracks(mood_music_pb2.Null())
        for soundtrack in soundtracks:
            if mood_id == soundtrack.mood_id:    
                filtered_mood_soundtrack_dict[soundtrack.id] = {
                    "name": soundtrack.name,
                    "mood_id": soundtrack.mood_id,
                    "tod_id": soundtrack.tod_id,
                    "song_ids": soundtrack.song_ids,
                }
        return filtered_mood_soundtrack_dict
    
    def get_liked_songs(self):
        """Returnes all liked songs from the logged in user"""
        if self.user== None:
            return "Not logged in."
        liked_songs_dict = {}
        # self.user.liked_songs
        songs = self.stub.ListSongs(mood_music_pb2.Null())
        for song in songs:
            if song.id in self.user.liked_songs:
                liked_songs_dict[song.id] = {
                    "trackName": song.trackName,
                    "artistName": song.artistName,
                    "artistId": song.artistId,
                    "genreId": song.genreId,
                    "releaseYear": song.releaseYear,
                }
        return liked_songs_dict

    def get_disliked_songs(self):
        """Returnes all disliked songs from the logged in user"""
        if self.user== None:
            return "Not logged in."
        disliked_songs_dict = {}
        songs = self.stub.ListSongs(mood_music_pb2.Null())
        for song in songs:
            if song.id in self.user.disliked_songs:
                disliked_songs_dict[song.id] = {
                    "trackName": song.trackName,
                    "artistName": song.artistName,
                    "artistId": song.artistId,
                    "genreId": song.genreId,
                    "releaseYear": song.releaseYear,
                }
        return disliked_songs_dict
    
    def generate_soundtrack(self, req_mood_id, req_tod_id):
        self.generated_soundtrack = None
        # if self.user == None: # check if the user is logged in.
        #     return "Not logged in!"
        # Check if mood exists
        all_moods = self.get_all_moods()
        if req_mood_id not in all_moods:
            raise ValueError(f"Mood with ID {req_mood_id} does not exist.")
        # Check if tod exists
        all_tods = self.get_all_tods()
        if req_tod_id not in all_tods:
            raise ValueError(f"Time of day with ID {req_tod_id} does not exist.")
        generator = mood_music_pb2.Generator(
            AID=0,
            mood_id=req_mood_id,
            tod_id=req_tod_id
        )
        self.generated_soundtrack = self.stub.GenerateSoundtrack(generator)
        print(self.generated_soundtrack)
        print(self.generated_soundtrack.song_ids)
        return

    def recommend_similar_songs(self, song_id):
        try:
            response = self.stub.RecommendSimilarSongs(mood_music_pb2.SongID(id=song_id))
            return response.songs
        
        except grpc.RpcError as e:
            print(f"Error code: {e._state.code}")

    def recommend_songs_like_soundtrack(self, soundtrack_id):
        try:
            response = self.stub.RecommendSongsLikeSoundtrack(mood_music_pb2.SoundtrackID(id=soundtrack_id))
            return response.songs
        
        except grpc.RpcError as e:
            print(f"Error code: {e._state.code}")

    def log_in(self, username):
        try:
            username = mood_music_pb2.AccountUsername(username=username)
            #password = mood_music_pb2.Account(password=password)
            self.user = self.stub.Login(username)
            return self.user
        except grpc.RpcError as e:
            raise grpc.RpcError
        
    def login_details(self):
        if self.user is None:
            return "Not logged in."
        return self.user
        
    def log_out(self):
        self.user = None
    

    def clear_server(self):
        """Resets server"""
        self.stub.Clear(mood_music_pb2.Null())

def run(): # pragma: no cover
    mmClient = MoodMusicClient()
    mmClient.clear_server()
    # try:
    #     print(mmClient.stub.GenerateSoundtrack(mood_music_pb2.Generator(AID=0, mood_id=1, tod_id=0)))
    #     print(mmClient.stub.GenerateWeatherSoundtrack(mood_music_pb2.AccountID(id=0)))
    # except grpc.RpcError as e:
    #     print(e.details())

    


    # try:
    #     soundtrack_id = 1
    #     recommended_songs = mmClient.recommend_songs_like_soundtrack(soundtrack_id)

    #     if recommended_songs:
    #         print(f"recommended songs for {soundtrack_id}")
    #         for song in recommended_songs:
    #             print(f"Song ID: {song.id}, Track Year: {song.releaseYear}")
    #     else:
    #         print("No recommendations found.")

    # except grpc.RpcError as e:
    #     print(f"Error code: {e.code()}")
    #     print(f"Error details: {e.details()}")
            
    try:
    # Example of using the recommend_similar_songs method
        song_id = 4  # Replace with the actual song ID you want recommendations for
        recommended_songs = mmClient.recommend_similar_songs(song_id)

        # Print the recommended songs
        if recommended_songs:
            print(f"Recommended Songs for {song_id}:")
            for song in recommended_songs:
                print(f"Song ID: {song.id}, Track Year: {song.releaseYear}, Artist: {song.artistId}, Genre ID: {song.genreId}, Score: {song.score}")
        else:
            print("No recommendations found.")
            
    except grpc.RpcError as e:
        print(f"Error code: {e.code()}")
        print(f"Error details: {e.details()}")


    # for i in range(len(list(mmClient.stub.ListSongs(mood_music_pb2.Null())))):
    #     mmClient.stub.SongSurvey(mood_music_pb2.Survey(AID=0,SID=i,MID=random.randint(0,9), TID=0))

    # for i in range(len(list(mmClient.stub.ListSongs(mood_music_pb2.Null())))):
    #     mmClient.stub.RateSong(mood_music_pb2.SongRating(AID=0, SID=i, liked=random.choice([True, False])))


    # print("-------------- GetSong --------------")
    # print(mmClient.get_one_song(1))
    # print("-------------- ListSongs --------------")
    # print(mmClient.get_all_songs())
    # print("-------------- ListMoods --------------")
    # print(mmClient.get_all_moods())
    # print("-------------- ListTODs --------------")
    # print(mmClient.get_all_tods())

    # print("-------------- ListSoundtracks --------------")
    # print(mmClient.get_all_soundtracks())

    # mmClient.create_song(mood_music_pb2.Song(
    #     trackName="Example",
    #     artistName="Example",
    #     artistId=3,
    #     genreId=2,
    #     releaseYear=2000
    # ))
    # print(mood_music_pb2.Soundtrack(
    #     id=1,
    #     name="Sad Time",
    #     mood_id=2,
    #     tod_id=1,
    #     song_ids=[1, 2, 3]
    # ))
    # print("-------------- ListSongs --------------")
    # print(mmClient.get_all_songs())
    # print(mmClient.add_song(1, 10))

    # mmClient.remove_soundtrack(1)

    # # Test the select_mood and select_tod functions
    # print("-------------- Select mood and tod --------------")
    # selected_mood = mmClient.select_mood()
    # print(f"Selected Mood ID: {selected_mood}")

    # selected_tod = mmClient.select_tod()
    # print(f"Selected Time of Day ID: {selected_tod}")

    # # test the log in and log out functions
    # mmClient.log_in("ad tempor sint ipsum", "test")
    # print(mmClient.login_details())
    # mmClient.log_out()
    # print(mmClient.login_details())

if __name__ == "__main__":
    # logging.basicConfig()
    run()
    # print(mood_music_pb2.Generator(AID=0, req_mood=mood_music_pb2.GeneratorMood(use=True, MID=1), req_tod=mood_music_pb2.GeneratorTOD(use=False, TID=1)))
    