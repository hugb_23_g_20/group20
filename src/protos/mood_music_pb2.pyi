from google.protobuf.internal import containers as _containers
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class Null(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class MoodTOD(_message.Message):
    __slots__ = ["mood_id", "tod_id"]
    MOOD_ID_FIELD_NUMBER: _ClassVar[int]
    TOD_ID_FIELD_NUMBER: _ClassVar[int]
    mood_id: int
    tod_id: int
    def __init__(self, mood_id: _Optional[int] = ..., tod_id: _Optional[int] = ...) -> None: ...

class RemoveSurveyRequest(_message.Message):
    __slots__ = ["AID", "SID"]
    AID_FIELD_NUMBER: _ClassVar[int]
    SID_FIELD_NUMBER: _ClassVar[int]
    AID: int
    SID: int
    def __init__(self, AID: _Optional[int] = ..., SID: _Optional[int] = ...) -> None: ...

class SongID(_message.Message):
    __slots__ = ["id"]
    ID_FIELD_NUMBER: _ClassVar[int]
    id: int
    def __init__(self, id: _Optional[int] = ...) -> None: ...

class SoundtrackID(_message.Message):
    __slots__ = ["id"]
    ID_FIELD_NUMBER: _ClassVar[int]
    id: int
    def __init__(self, id: _Optional[int] = ...) -> None: ...

class AccountID(_message.Message):
    __slots__ = ["id"]
    ID_FIELD_NUMBER: _ClassVar[int]
    id: int
    def __init__(self, id: _Optional[int] = ...) -> None: ...

class MoodID(_message.Message):
    __slots__ = ["id"]
    ID_FIELD_NUMBER: _ClassVar[int]
    id: int
    def __init__(self, id: _Optional[int] = ...) -> None: ...

class TODID(_message.Message):
    __slots__ = ["id"]
    ID_FIELD_NUMBER: _ClassVar[int]
    id: int
    def __init__(self, id: _Optional[int] = ...) -> None: ...

class AccountUsername(_message.Message):
    __slots__ = ["username"]
    USERNAME_FIELD_NUMBER: _ClassVar[int]
    username: str
    def __init__(self, username: _Optional[str] = ...) -> None: ...

class Survey(_message.Message):
    __slots__ = ["AID", "SID", "MID", "TID"]
    AID_FIELD_NUMBER: _ClassVar[int]
    SID_FIELD_NUMBER: _ClassVar[int]
    MID_FIELD_NUMBER: _ClassVar[int]
    TID_FIELD_NUMBER: _ClassVar[int]
    AID: int
    SID: int
    MID: int
    TID: int
    def __init__(self, AID: _Optional[int] = ..., SID: _Optional[int] = ..., MID: _Optional[int] = ..., TID: _Optional[int] = ...) -> None: ...

class SongRating(_message.Message):
    __slots__ = ["AID", "SID", "liked"]
    AID_FIELD_NUMBER: _ClassVar[int]
    SID_FIELD_NUMBER: _ClassVar[int]
    LIKED_FIELD_NUMBER: _ClassVar[int]
    AID: int
    SID: int
    liked: bool
    def __init__(self, AID: _Optional[int] = ..., SID: _Optional[int] = ..., liked: bool = ...) -> None: ...

class Generator(_message.Message):
    __slots__ = ["AID", "mood_id", "tod_id"]
    AID_FIELD_NUMBER: _ClassVar[int]
    MOOD_ID_FIELD_NUMBER: _ClassVar[int]
    TOD_ID_FIELD_NUMBER: _ClassVar[int]
    AID: int
    mood_id: int
    tod_id: int
    def __init__(self, AID: _Optional[int] = ..., mood_id: _Optional[int] = ..., tod_id: _Optional[int] = ...) -> None: ...

class FilterParameter(_message.Message):
    __slots__ = ["filter_parameter", "id", "user_id"]
    FILTER_PARAMETER_FIELD_NUMBER: _ClassVar[int]
    ID_FIELD_NUMBER: _ClassVar[int]
    USER_ID_FIELD_NUMBER: _ClassVar[int]
    filter_parameter: str
    id: int
    user_id: int
    def __init__(self, filter_parameter: _Optional[str] = ..., id: _Optional[int] = ..., user_id: _Optional[int] = ...) -> None: ...

class TimeOfDay(_message.Message):
    __slots__ = ["id", "descriptor"]
    ID_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTOR_FIELD_NUMBER: _ClassVar[int]
    id: int
    descriptor: str
    def __init__(self, id: _Optional[int] = ..., descriptor: _Optional[str] = ...) -> None: ...

class Mood(_message.Message):
    __slots__ = ["id", "descriptor"]
    ID_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTOR_FIELD_NUMBER: _ClassVar[int]
    id: int
    descriptor: str
    def __init__(self, id: _Optional[int] = ..., descriptor: _Optional[str] = ...) -> None: ...

class Song(_message.Message):
    __slots__ = ["id", "trackName", "artistName", "artistId", "genreId", "releaseYear", "score"]
    ID_FIELD_NUMBER: _ClassVar[int]
    TRACKNAME_FIELD_NUMBER: _ClassVar[int]
    ARTISTNAME_FIELD_NUMBER: _ClassVar[int]
    ARTISTID_FIELD_NUMBER: _ClassVar[int]
    GENREID_FIELD_NUMBER: _ClassVar[int]
    RELEASEYEAR_FIELD_NUMBER: _ClassVar[int]
    SCORE_FIELD_NUMBER: _ClassVar[int]
    id: int
    trackName: str
    artistName: str
    artistId: int
    genreId: int
    releaseYear: int
    score: int
    def __init__(self, id: _Optional[int] = ..., trackName: _Optional[str] = ..., artistName: _Optional[str] = ..., artistId: _Optional[int] = ..., genreId: _Optional[int] = ..., releaseYear: _Optional[int] = ..., score: _Optional[int] = ...) -> None: ...

class SongList(_message.Message):
    __slots__ = ["songs"]
    SONGS_FIELD_NUMBER: _ClassVar[int]
    songs: _containers.RepeatedCompositeFieldContainer[Song]
    def __init__(self, songs: _Optional[_Iterable[_Union[Song, _Mapping]]] = ...) -> None: ...

class Soundtrack(_message.Message):
    __slots__ = ["id", "name", "mood_id", "tod_id", "song_ids", "user_id"]
    ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    MOOD_ID_FIELD_NUMBER: _ClassVar[int]
    TOD_ID_FIELD_NUMBER: _ClassVar[int]
    SONG_IDS_FIELD_NUMBER: _ClassVar[int]
    USER_ID_FIELD_NUMBER: _ClassVar[int]
    id: int
    name: str
    mood_id: int
    tod_id: int
    song_ids: _containers.RepeatedScalarFieldContainer[int]
    user_id: int
    def __init__(self, id: _Optional[int] = ..., name: _Optional[str] = ..., mood_id: _Optional[int] = ..., tod_id: _Optional[int] = ..., song_ids: _Optional[_Iterable[int]] = ..., user_id: _Optional[int] = ...) -> None: ...

class Account(_message.Message):
    __slots__ = ["id", "username", "liked_songs", "disliked_songs", "soundtracks"]
    ID_FIELD_NUMBER: _ClassVar[int]
    USERNAME_FIELD_NUMBER: _ClassVar[int]
    LIKED_SONGS_FIELD_NUMBER: _ClassVar[int]
    DISLIKED_SONGS_FIELD_NUMBER: _ClassVar[int]
    SOUNDTRACKS_FIELD_NUMBER: _ClassVar[int]
    id: int
    username: str
    liked_songs: _containers.RepeatedScalarFieldContainer[int]
    disliked_songs: _containers.RepeatedScalarFieldContainer[int]
    soundtracks: _containers.RepeatedScalarFieldContainer[int]
    def __init__(self, id: _Optional[int] = ..., username: _Optional[str] = ..., liked_songs: _Optional[_Iterable[int]] = ..., disliked_songs: _Optional[_Iterable[int]] = ..., soundtracks: _Optional[_Iterable[int]] = ...) -> None: ...

class RecommendationRequest(_message.Message):
    __slots__ = ["song_id", "user_id", "soundtrack_id"]
    SONG_ID_FIELD_NUMBER: _ClassVar[int]
    USER_ID_FIELD_NUMBER: _ClassVar[int]
    SOUNDTRACK_ID_FIELD_NUMBER: _ClassVar[int]
    song_id: int
    user_id: int
    soundtrack_id: int
    def __init__(self, song_id: _Optional[int] = ..., user_id: _Optional[int] = ..., soundtrack_id: _Optional[int] = ...) -> None: ...
