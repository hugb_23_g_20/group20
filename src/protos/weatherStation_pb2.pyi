from google.protobuf.internal import containers as _containers
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class HistoryPlace(_message.Message):
    __slots__ = ["location_descriptor", "start_time", "end_time", "api_key"]
    LOCATION_DESCRIPTOR_FIELD_NUMBER: _ClassVar[int]
    START_TIME_FIELD_NUMBER: _ClassVar[int]
    END_TIME_FIELD_NUMBER: _ClassVar[int]
    API_KEY_FIELD_NUMBER: _ClassVar[int]
    location_descriptor: str
    start_time: int
    end_time: int
    api_key: str
    def __init__(self, location_descriptor: _Optional[str] = ..., start_time: _Optional[int] = ..., end_time: _Optional[int] = ..., api_key: _Optional[str] = ...) -> None: ...

class Location(_message.Message):
    __slots__ = ["lat", "lon", "api_key"]
    LAT_FIELD_NUMBER: _ClassVar[int]
    LON_FIELD_NUMBER: _ClassVar[int]
    API_KEY_FIELD_NUMBER: _ClassVar[int]
    lat: float
    lon: float
    api_key: str
    def __init__(self, lat: _Optional[float] = ..., lon: _Optional[float] = ..., api_key: _Optional[str] = ...) -> None: ...

class MultiForecast(_message.Message):
    __slots__ = ["forecast"]
    FORECAST_FIELD_NUMBER: _ClassVar[int]
    forecast: _containers.RepeatedCompositeFieldContainer[WeatherForecast]
    def __init__(self, forecast: _Optional[_Iterable[_Union[WeatherForecast, _Mapping]]] = ...) -> None: ...

class WeatherForecast(_message.Message):
    __slots__ = ["temp", "humidity", "clouds", "wind", "windDeg", "dtTxt"]
    TEMP_FIELD_NUMBER: _ClassVar[int]
    HUMIDITY_FIELD_NUMBER: _ClassVar[int]
    CLOUDS_FIELD_NUMBER: _ClassVar[int]
    WIND_FIELD_NUMBER: _ClassVar[int]
    WINDDEG_FIELD_NUMBER: _ClassVar[int]
    DTTXT_FIELD_NUMBER: _ClassVar[int]
    temp: float
    humidity: int
    clouds: int
    wind: float
    windDeg: int
    dtTxt: str
    def __init__(self, temp: _Optional[float] = ..., humidity: _Optional[int] = ..., clouds: _Optional[int] = ..., wind: _Optional[float] = ..., windDeg: _Optional[int] = ..., dtTxt: _Optional[str] = ...) -> None: ...

class CurrentWeather(_message.Message):
    __slots__ = ["temp", "humidity", "clouds", "wind", "windDeg"]
    TEMP_FIELD_NUMBER: _ClassVar[int]
    HUMIDITY_FIELD_NUMBER: _ClassVar[int]
    CLOUDS_FIELD_NUMBER: _ClassVar[int]
    WIND_FIELD_NUMBER: _ClassVar[int]
    WINDDEG_FIELD_NUMBER: _ClassVar[int]
    temp: float
    humidity: int
    clouds: int
    wind: float
    windDeg: int
    def __init__(self, temp: _Optional[float] = ..., humidity: _Optional[int] = ..., clouds: _Optional[int] = ..., wind: _Optional[float] = ..., windDeg: _Optional[int] = ...) -> None: ...
