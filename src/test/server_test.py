import grpc
from app import mood_music_server
from protos import mood_music_pb2, weatherStation_pb2
import unittest
        
class MoodMusicTest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.server = mood_music_server.SongDBServicer(True)
    @classmethod
    def tearDownClass(self):
        self.server.Clear(mood_music_pb2.Null(), None)

    def testAllSongs(self):
        """Test that server.songs yields same result as ListSongs and that the results are correct"""
        songs = self.server.songs
        list_songs = [item for item in self.server.ListSongs(mood_music_pb2.Null(),None)]
        self.assertEqual(songs, list_songs)
        self.assertEqual(len(songs),len(list_songs))

    def testOneSong(self):
        """Test that server.songs[0] (id = 1) yields same result as GetSong(1) and that the results are correct"""
        song = self.server.songs[0]
        get_song = self.server.GetSong(mood_music_pb2.SongID(id=0), None)
        self.assertEqual(song,get_song)
    
    def testAllSoundtracks(self):
        """Test that server.soundtrack yields same result as ListSoundtracks and that the results are correct"""
        soundtracks = self.server.soundtracks
        list_soundtracks = [item for item in self.server.ListSoundtracks(mood_music_pb2.Null(),None)]
        self.assertEqual(soundtracks, list_soundtracks)
        self.assertEqual(len(soundtracks), 3)  

    def testOneSoundtrack(self):
        """Test that server.soundtrack[0] (id = 1) yields same result as GetSoundtrack(1) and that the results are correct"""
        soundtrack = self.server.soundtracks[0]
        get_soundtrack = self.server.GetSoundtrack(mood_music_pb2.SoundtrackID(id=0), None)
        self.assertEqual(soundtrack, get_soundtrack)
        self.assertEqual(soundtrack.id, 0)
        self.assertEqual(soundtrack.name,"Mellow Evening")
        self.assertEqual(soundtrack.mood_id, 1)
        self.assertEqual(soundtrack.tod_id, 0)

    def testAllMoods(self):
        """Test that server.moods yields same result as ListMoods and that the results are correct"""
        moods = self.server.moods
        list_moods = [item for item in self.server.ListMoods(mood_music_pb2.Null(),None)]
        self.assertEqual(moods, list_moods)
        self.assertEqual(len(moods), len(list_moods))

    def testAllTODS(self):
        """Test that server.tods yields same result as ListTODs and that the results are correct"""
        tods = self.server.tods
        list_tods = [item for item in self.server.ListTODs(mood_music_pb2.Null(),None)]
        self.assertEqual(tods, list_tods)
        self.assertEqual(len(tods), len(list_tods))
        self.assertEqual(tods, list_tods)

    def testCreateSoundtrack(self):
        """Test if CreateSoundtrack works, yields correct results, and saves to the server"""
        soundtracks_length = len(self.server.soundtracks)
        soundtrack = self.server.CreateSoundtrack(mood_music_pb2.Soundtrack(
            name = "Sad Time",
            user_id=0,
            mood_id = 1,
            tod_id = 0,
        ), None)
        self.assertEqual(soundtrack.id, 3)
        self.assertEqual(soundtrack.mood_id,1)
        self.assertEqual(soundtrack.tod_id,0)
        self.assertEqual(self.server.GetSoundtrack(mood_music_pb2.SoundtrackID(id=3), None), soundtrack)
        self.assertEqual(len(self.server.soundtracks), soundtracks_length + 1)
        soundtrack = self.server.CreateSoundtrack(mood_music_pb2.Soundtrack(
            name = "Sad Time",
            user_id=0,
            mood_id = 1,
            tod_id = 0,
        ), None)

    def testEditSoundtrack(self):
        """Tests that you can change a soundtrack, yields correct results, and the changes are saved"""  
        soundtrack_item = mood_music_pb2.Soundtrack(
            id = 0,
            user_id=0,
            name = "Sad Time",
            mood_id = 0,
            tod_id = 1,
            song_ids = [0,1,2,3]
        )
        soundtrack = self.server.EditSoundtrack(soundtrack_item, None)
        self.assertEqual(soundtrack, soundtrack_item)
        self.assertEqual(self.server.GetSoundtrack(mood_music_pb2.SoundtrackID(id=0), None), soundtrack)

        soundtrack_item = mood_music_pb2.Soundtrack(
            id = 0,
            user_id=0,
            name = "Mellow Evening",
            mood_id = 1,
            tod_id = 0,
        )
        soundtrack = self.server.EditSoundtrack(soundtrack_item, None)
        self.assertEqual(soundtrack, soundtrack_item)
        self.assertEqual(self.server.GetSoundtrack(mood_music_pb2.SoundtrackID(id=0), None), soundtrack)

    def testRemoveSoundtrack(self):
        """Test that the soundtrack with id 4 gets removed from the server"""
        soundtrack_length = len(self.server.soundtracks)
        soundtrack = self.server.RemoveSoundtrack(mood_music_pb2.SoundtrackID(id=3), None)
        self.assertEqual(soundtrack.id, 3)
        self.assertEqual(len(self.server.soundtracks), soundtrack_length - 1)

    def testCreateAccount(self):
        """Test the valid inputss of CreateAccount"""
        account_length = len(self.server.accounts)
        account = self.server.CreateAccount(mood_music_pb2.Account(username = "User 1"), None)
        self.assertEqual(account.id, 1)
        self.assertEqual(self.server.GetAccount(mood_music_pb2.AccountID(id=1), None), account)
        self.assertEqual(len(self.server.accounts), account_length + 1)

    def testEditAccount(self):
        """Tests that you can change an account, yields correct results, and the changes are saved"""  
        account_item = mood_music_pb2.Account(
            id = 1,
            username= "Test"
        )
        account = self.server.EditAccount(account_item, None)
        self.assertEqual(account.username, account_item.username)
        self.assertEqual(self.server.GetAccount(mood_music_pb2.AccountID(id=1), None), account)
    
    def testRemoveAccount(self):
        """Test that the account with id 1 gets removed from the server"""
        account_length = len(self.server.accounts)
        account = self.server.RemoveAccount(mood_music_pb2.AccountID(id=1), None)
        self.assertEqual(account.id, 1)
        self.assertEqual(len(self.server.accounts), account_length - 1)

    def testLogin(self):
        """Test logging in with a username"""
        user = self.server.Login(mood_music_pb2.AccountUsername(username="ad tempor sint ipsum"), None)
        user_test = self.server.GetAccount(mood_music_pb2.AccountID(id=0), None)
        self.assertEqual(user.id, 0)
        self.assertEqual(user.liked_songs, user_test.liked_songs)
        self.assertEqual(user.disliked_songs, user_test.disliked_songs)
        self.assertEqual(user.soundtracks, list(user_test.soundtracks))


    def testSongSurvey(self):
        """Tests that song survey records the survey and can edit it"""
        user_survey_length = len([item for item in self.server.ListSurveys(mood_music_pb2.AccountID(id=0), None)])
        survey_item = mood_music_pb2.Survey(AID=0, SID=2, MID=0, TID=0)
        survey = self.server.SongSurvey(survey_item, None)
        self.assertEqual(survey, survey_item)
        # all_user_surveys = [item for item in self.server.ListSurveys(mood_music_pb2.AccountID(id=0), None)]
        # self.assertEqual(len(all_user_surveys), user_survey_length + 1)

    def testRateSong(self):
        """Test the ability to rate a song"""
        account = self.server.GetAccount(mood_music_pb2.AccountID(id=0), None)
        liked_before, disliked_before = list(account.liked_songs), list(account.disliked_songs)

        rating = mood_music_pb2.SongRating(AID=0, SID=48, liked=True)
        account_new = self.server.RateSong(rating, None)
        liked_after, disliked_after = list(account_new.liked_songs), list(account_new.disliked_songs)
        self.assertTrue(rating.SID in liked_after)
        self.assertEqual(len(liked_before) + 1, len(liked_after))
        self.assertEqual(len(disliked_before), len(disliked_after))

        rating = mood_music_pb2.SongRating(AID=0, SID=48, liked=False)
        account_new = self.server.RateSong(rating, None)
        liked_after, disliked_after = list(account_new.liked_songs), list(account_new.disliked_songs)
        self.assertTrue(rating.SID in disliked_after)
        self.assertEqual(len(liked_before), len(liked_after))
        self.assertEqual(len(disliked_before) + 1, len(disliked_after))

        rating = mood_music_pb2.SongRating(AID=0, SID=48, liked=True)
        account_new = self.server.RateSong(rating, None)
        liked_after, disliked_after = list(account_new.liked_songs), list(account_new.disliked_songs)
        self.assertTrue(rating.SID in liked_after)
        self.assertEqual(len(liked_before) + 1, len(liked_after))
        self.assertEqual(len(disliked_before), len(disliked_after))

    def testGenerateSoundtrack(self):
        """Tests that default generated weather soundtrack is the same as generating for mood 2"""
        generated_soundtrack = self.server.GenerateSoundtrack(mood_music_pb2.Generator(AID=0, mood_id=2,tod_id=0), None)
        generated_weather_soundtrack = self.server.GenerateWeatherSoundtrack(mood_music_pb2.AccountID(id=0), None)
        self.assertEqual(generated_soundtrack.user_id, generated_weather_soundtrack.user_id)
        self.assertEqual(generated_soundtrack.mood_id, generated_weather_soundtrack.mood_id)
        self.assertEqual(generated_soundtrack.tod_id, generated_weather_soundtrack.tod_id)
        self.assertEqual(generated_soundtrack.song_ids, generated_weather_soundtrack.song_ids)
        generated_soundtrack = self.server.GenerateSoundtrack(mood_music_pb2.Generator(AID=0, mood_id=6,tod_id=6), None)
        self.assertEqual(generated_soundtrack.user_id, 0)
        self.assertEqual(generated_soundtrack.mood_id, 6)
        self.assertEqual(generated_soundtrack.tod_id, 6)
        generated_soundtrack = self.server.GenerateSoundtrack(mood_music_pb2.Generator(AID=0, mood_id=0,tod_id=1), None)
        self.assertEqual(generated_soundtrack.user_id, 0)
        self.assertEqual(generated_soundtrack.mood_id, 0)
        self.assertEqual(generated_soundtrack.tod_id, 1)
        generated_soundtrack = self.server.GenerateSoundtrack(mood_music_pb2.Generator(AID=0, mood_id=0,tod_id=0), None)
        self.assertEqual(generated_soundtrack.user_id, 0)
        self.assertEqual(generated_soundtrack.mood_id, 0)
        self.assertEqual(generated_soundtrack.tod_id, 0)

    def testRecommendSimilarSongs(self):
        """Test the recommendation of similar songs for a given song ID"""
        song_id = 0
        response = self.server.RecommendSimilarSongs(mood_music_pb2.SongID(id=song_id), None)
        self.assertTrue(response.songs)

        for song in response.songs:
            self.assertGreaterEqual(song.score, 1)

    def testRecommendSongsForUser(self):
        """Test that recommend_songs_for_user returns expected results."""
        user_id = 0  #change to the ID you want
        recommended_songs = self.server.RecommendSongsForUser(mood_music_pb2.AccountID(id=user_id), None)
        recommended_song_ids = {song.id for song in recommended_songs.songs}
        self.assertEqual(len(recommended_song_ids), 5) #testing that the recommendations should return 5 songs. Change it according to how many songs you want to recommend
        # Optionally, check that the songs have a certain minimum similarity score
        for song in recommended_songs.songs:
            self.assertGreaterEqual(song.score, 2)  #Using the score 2. Change it to whatever you want the minimum similarity score to be

    def testRecommendSongsLikeSoundtrack(self):
        """Test soundtrack recommendation errors"""
        recommended_songs = self.server.RecommendSongsLikeSoundtrack(mood_music_pb2.SoundtrackID(id=7), None)
        self.assertEqual(len(recommended_songs.songs), 3)
        for song in recommended_songs.songs:
            self.assertTrue(song.id in [20, 36, 40])

    def testFilter(self):
        """Tests the filter for correct functionality"""
        user = self.server.GetAccount(mood_music_pb2.AccountID(id=0), None)
        filtered_songs = self.server.Filter(mood_music_pb2.FilterParameter(filter_parameter="mood", id=1, user_id=0), None)
        self.assertEqual(len(filtered_songs), 3)
        for song in filtered_songs:
            self.assertTrue(song.id in user.liked_songs)
        filtered_songs = self.server.Filter(mood_music_pb2.FilterParameter(filter_parameter="tod", id=1, user_id=0), None)
        self.assertEqual(len(filtered_songs), 2)
        for song in filtered_songs:
            self.assertTrue(song.id in user.liked_songs)

    def testFilterErrors(self):
        """Tests the filter error conditions"""
        server, stub = mood_music_server.test_server_start()
        with self.assertRaises(grpc.RpcError) as e:
            stub.Filter(mood_music_pb2.FilterParameter(filter_parameter="", id=1, user_id=0))
        self.assertEqual(e.exception.code(), grpc.StatusCode.INVALID_ARGUMENT)
        self.assertEqual(e.exception.details(), "filter_paramater was not found")
        with self.assertRaises(grpc.RpcError) as e:
            stub.Filter(mood_music_pb2.FilterParameter(filter_parameter="mood", id=1, user_id=100))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), "Account with id 100 was not found")
        with self.assertRaises(grpc.RpcError) as e:
            stub.Filter(mood_music_pb2.FilterParameter(filter_parameter="tod", id=100, user_id=0))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), "TOD with id: 100 not found.")
        with self.assertRaises(grpc.RpcError) as e:
            stub.Filter(mood_music_pb2.FilterParameter(filter_parameter="mood", id=100, user_id=0))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), "Mood with id: 100 not found.")
        mood_music_server.test_server_stop(server)

    def testRecommendSongsLikeSoundtrackErrors(self):
        """Testing error for soundtrack with id that is not in the database"""
        server, stub = mood_music_server.test_server_start()
        with self.assertRaises(grpc.RpcError) as e:
            stub.RecommendSongsLikeSoundtrack(mood_music_pb2.SoundtrackID(id=100))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f'Soundtrack with id: 100 not found.')
        mood_music_server.test_server_stop(server)

    def testRateSongErrors(self):
        """Testing error for soundtrack with id that is not in the database"""
        server, stub = mood_music_server.test_server_start()
        with self.assertRaises(grpc.RpcError) as e:
            stub.RateSong(mood_music_pb2.SongRating(AID=2))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f'Account with ID 2 was not found.')
        mood_music_server.test_server_stop(server)

    
    def testRecommendSimilarSongsErrors(self):
        """Testing error for song with id that is not in the database"""
        server, stub = mood_music_server.test_server_start()  
        with self.assertRaises(grpc.RpcError) as e:
            stub.RecommendSimilarSongs(mood_music_pb2.SongID(id=1000))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f'Song with id: 1000 not found.')        
        mood_music_server.test_server_stop(server)

    def testGetSongErrors(self):
        """Testing error for song with id that is not in the database"""
        server, stub = mood_music_server.test_server_start()  
        with self.assertRaises(grpc.RpcError) as e:
            stub.GetSong(mood_music_pb2.SongID(id=10000))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f'Song with id:10000 not found.')
        mood_music_server.test_server_stop(server)

    def testSoundtrackErrors(self):
        """Testing potential errors in get, create, edit, remove soundtrack"""
        server, stub = mood_music_server.test_server_start()  
        
        with self.assertRaises(grpc.RpcError) as e:
            stub.GetSoundtrack(mood_music_pb2.SoundtrackID(id=10))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)

        with self.assertRaises(grpc.RpcError) as e:
            stub.CreateSoundtrack(mood_music_pb2.Soundtrack(
                name = "Sad Time",
                mood_id = 0,
                tod_id = 0,
                user_id= 1,
            ))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f"Account ID: 1 not found")

        with self.assertRaises(grpc.RpcError) as e:
            stub.CreateSoundtrack(mood_music_pb2.Soundtrack(
                name = "Sad Time",
                mood_id = 0,
                tod_id = 0,
                song_ids = [1,1000],
            ))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f"Song ID: 1000 not found")
        
        with self.assertRaises(grpc.RpcError) as e:
            stub.CreateSoundtrack(mood_music_pb2.Soundtrack(
                name = "Sad Time",
                mood_id = 10,
                tod_id = 0,
            ))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f"Mood ID: 10 not found.")

        with self.assertRaises(grpc.RpcError) as e:
            stub.CreateSoundtrack(mood_music_pb2.Soundtrack(
                name = "Sad Time",
                mood_id = 0,
                tod_id = 10,
            ))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f"Time of Day ID: 10 not found")

        with self.assertRaises(grpc.RpcError) as e:
            stub.EditSoundtrack(mood_music_pb2.Soundtrack(
                mood_id = 10,
            ))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f"Mood ID: 10 not found.")

        with self.assertRaises(grpc.RpcError) as e:
            stub.EditSoundtrack(mood_music_pb2.Soundtrack(
                tod_id = 10,
            ))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f"Time of Day ID: 10 not found")

        with self.assertRaises(grpc.RpcError) as e:
            stub.EditSoundtrack(mood_music_pb2.Soundtrack(
                song_ids= [1,1000],
            ))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f"Song ID: 1000 not found")

        with self.assertRaises(grpc.RpcError) as e:
            stub.EditSoundtrack(mood_music_pb2.Soundtrack(
                id = 100
            ))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f"Soundtrack with id:100 not found.")

        with self.assertRaises(grpc.RpcError) as e:
            stub.RemoveSoundtrack(mood_music_pb2.Soundtrack(
                id = 100
            ))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f"Soundtrack with id:100 not found.")

        mood_music_server.test_server_stop(server)

    def testGenerateSoundtrackErrors(self):
        """Testing potential errors in GenerateSoundtrack"""
        server, stub = mood_music_server.test_server_start()  
        with self.assertRaises(grpc.RpcError) as e:
            stub.GenerateSoundtrack(mood_music_pb2.Generator(
                mood_id=10
            ))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f'Mood ID: 10 not found.')

        with self.assertRaises(grpc.RpcError) as e:
            stub.GenerateSoundtrack(mood_music_pb2.Generator(
                tod_id=10
            ))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f'Time of Day ID: 10 not found')

        with self.assertRaises(grpc.RpcError) as e:
            stub.GenerateSoundtrack(mood_music_pb2.Generator(
                AID=10
            ))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f'Account ID: 10 not found')

        mood_music_server.test_server_stop(server)      

    def testAccountErrors(self):
        """Testing errors in get, create, edit and remove account"""
        server, stub = mood_music_server.test_server_start()
        with self.assertRaises(grpc.RpcError) as e:
            stub.GetAccount(mood_music_pb2.AccountID(id=10))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f"Account with id 10 was not found")

        with self.assertRaises(grpc.RpcError) as e:
            stub.CreateAccount(mood_music_pb2.Account())
        self.assertEqual(e.exception.code(), grpc.StatusCode.INVALID_ARGUMENT)
        self.assertEqual(e.exception.details(), f'Username can not be empty')

        with self.assertRaises(grpc.RpcError) as e:
            stub.EditAccount(mood_music_pb2.Account(id=100))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f'Account with id 100 was not found')

        with self.assertRaises(grpc.RpcError) as e:
            stub.RemoveAccount(mood_music_pb2.Account(id=100))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f'Account with id 100 was not found')

        with self.assertRaises(grpc.RpcError) as e:
            stub.EditAccount(mood_music_pb2.Account(id=0,username=""))
        self.assertEqual(e.exception.code(), grpc.StatusCode.INVALID_ARGUMENT)
        self.assertEqual(e.exception.details(), f'Username cannot be empty')

        mood_music_server.test_server_stop(server)


    def testSurveyErrors(self):
        """Testing errors in SongSurvey"""
        server, stub = mood_music_server.test_server_start()
        with self.assertRaises(grpc.RpcError) as e:
            stub.SongSurvey(mood_music_pb2.Survey(AID=10, SID=2, MID=0, TID=0))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f'Account with id: 10 not found.')

        with self.assertRaises(grpc.RpcError) as e:
            stub.SongSurvey(mood_music_pb2.Survey(AID=0, SID=100, MID=0, TID=0))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f'Song with id: 100 not found.')

        with self.assertRaises(grpc.RpcError) as e:
            stub.SongSurvey(mood_music_pb2.Survey(AID=0, SID=0, MID=10, TID=0))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f'Mood with id: 10 not found.')

        with self.assertRaises(grpc.RpcError) as e:
            stub.SongSurvey(mood_music_pb2.Survey(AID=0, SID=0, MID=0, TID=10))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f'Time of Day with id: 10 not found.')

        with self.assertRaises(grpc.RpcError) as e:
            stub.RemoveSongSurvey(mood_music_pb2.RemoveSurveyRequest(AID=100, SID=100))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), 'Survey with AID 100 and SID 100 was not found')

        mood_music_server.test_server_stop(server)

    def testLoginError(self):
        """Testing errors in Login"""
        server, stub = mood_music_server.test_server_start()
        with self.assertRaises(grpc.RpcError) as e:
            stub.Login(mood_music_pb2.AccountUsername(username="ERROR"))
        self.assertEqual(e.exception.code(), grpc.StatusCode.NOT_FOUND)
        self.assertEqual(e.exception.details(), f'Account with username ERROR was not found')
        mood_music_server.test_server_stop(server)

    def testWeatherServiceConnection(self):
        """Tests if the weather service returns a response"""
        weather_data = self.server.request_current_weather()
        self.assertTrue(type(weather_data) is type(weatherStation_pb2.CurrentWeather()))

if __name__ == "__main__":  
    unittest.main()
