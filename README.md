First install the requirements using pip install < requirements.txt

To run go to the src folder and run this command

    python -m app.mood_music_server

In another terminal execute these commands 

    python -m app.mood_music_client

    Note: The client is not up to date, it was only used as a testing suite in the beginning of the project and the tests serve that function now.

To recompile protoc:

    python -m grpc_tools.protoc --python_out=protos/ --pyi_out=protos/ --grpc_python_out=protos/ -Iprotos/ mood_music.proto

    Change import in mood_music_pb2_grpc.py from import mood_music_pb2 to from . import mood_music_pb2

To check coverage and run these tests, run these commands from src directory in order:

    python -m coverage run --data-file=test/.coverage --source app -m unittest test.server_test
    
    python -m coverage report --data-file=test/.coverage

    python -m coverage html --directory=test/htmlcov --data-file=test/.coverage

    To see results from html coverage open the index.html file in browser, found under src/test/htmlcov


